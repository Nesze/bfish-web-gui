import { environment_common } from './environment.common';

export const environment = {
  production: false,
  protocol: 'https',
  domain: 'fs.boltz.hu',
  ...environment_common
};
