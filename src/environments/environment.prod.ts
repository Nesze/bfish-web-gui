import { environment_common } from './environment.common';

export const environment = {
  production: true,
  protocol: 'https',
  domain: 'fs.boltz.hu',
  ...environment_common
};
