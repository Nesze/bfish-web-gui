import XRegExp from 'xregexp';

export const environment_common = {
  applicationVersion: '0.4.0',
  apiUrl: 'api',
  userBaseUrl: 'user',
  filesBaseUrl: 'files',
  usernameRegExp: XRegExp('^(?!-)(?!.*-$)(?!.*--)[A-Za-z0-9-]{3,32}$', 'u'),
  passwordRegExp: XRegExp('^(?=.{8,}$)(?=.*[0-9])(?=.*\\p{Ll})(?=.*\\p{Lu}).*$', 'u'),
  maxFileSize: 107374182400
}
