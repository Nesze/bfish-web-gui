import { Component, OnInit, Inject } from '@angular/core';
import { Validators, UntypedFormBuilder } from '@angular/forms';
import { MustMatch } from 'src/app/utils/must-match';
import { PasswordRegExp } from 'src/app/services/tokens';
import { CommonService } from 'src/app/services/common.service';
import { UserService } from 'src/app/services/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { FieldErrorResponse } from 'src/app/models/dto/field-error-response';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss', '../authentication.scss']
})
export class ResetPasswordComponent implements OnInit {

  form = this.fb.group({
    newPassword: ['', [Validators.required, Validators.pattern(this.passwordRegExp)]],
    newPassword2: ['', [Validators.required]]
  }, {
    validator: MustMatch('newPassword', 'newPassword2')
  });

  private email?: string;
  private token?: string;

  constructor(
    public commonService: CommonService,
    private userService: UserService,
    private fb: UntypedFormBuilder,
    @Inject(PasswordRegExp) private passwordRegExp: string,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const email = this.route.snapshot.queryParamMap.get('email');
    const token = this.route.snapshot.queryParamMap.get('token');
    if (email == null || token == null) {
      this.commonService.showMessage('Invalid reset link');
      this.router.navigate(['/']);
      return;
    }
    
    this.email = email;
    this.token = encodeURIComponent(token);
  }

  // Private event handlers

  onSuccessfulPasswordChange(): void {
    this.commonService.showMessage('Password changed successfully');
  }

  onFailedPasswordChange(errorResponse: HttpErrorResponse): void {
    const error = errorResponse.error as FieldErrorResponse;
    if (error != null && error.field != null && error.field !== '') {
      switch (error.field) {
        case 'token':
          this.commonService.showMessage('Token is invalid (it might have expired)');
      this.router.navigate(['/']);
          return;
        case 'newPassword':
          this.form.controls.newPassword.setErrors({ invalid: true });
          return;
      }
    }
    this.commonService.showMessage('Password change failed');
  }

  // Template helpers

  getNewPasswordErrorMessage() {
    if (this.form.controls.newPassword.hasError('required')) {
      return 'Required';
    } else if (this.form.controls.newPassword.hasError('pattern')
      || this.form.controls.newPassword.hasError('invalid')) {
      return 'Invalid format';
    }
    return '';
  }

  getNewPassword2ErrorMessage() {
    if (this.form.controls.newPassword2.hasError('required')) {
      return 'Required';
    } else if (this.form.controls.newPassword2.hasError('pattern')) {
      return 'Invalid format';
    } else if (this.form.controls.newPassword2.hasError('mustMatch')) {
      return 'Passwords don\'t match';
    }
    return '';
  }

  // Template event handlers

  onSubmit(): void {
    if (!this.form.valid || this.email == null || this.token == null) {
      return;
    }
    const processId = this.commonService.loadingStart();
    this.userService.resetPassword(
      this.email,
      this.form.controls.newPassword.value,
      this.token
    ).pipe(
      map(() => {
        this.onSuccessfulPasswordChange();
        return true;
      }),
      catchError((error: HttpErrorResponse) => {
        this.onFailedPasswordChange(error);
        return of(false);
      })
    ).subscribe(() => {
      this.commonService.loadingEnd(processId);
    });
  }

}
