import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, Validators, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { switchMap, map, takeWhile, take, catchError } from 'rxjs/operators';
import { UserDetails } from 'src/app/models/dto/user-details';
import { MustMatch } from 'src/app/utils/must-match';
import { UsernameRegExp, PasswordRegExp } from 'src/app/services/tokens';
import { of } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { FieldErrorResponse, FieldErrorType } from 'src/app/models/dto/field-error-response';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss', '../authentication.scss']
})
export class RegistrationComponent implements OnInit {

  registrationForm: UntypedFormGroup;

  constructor(
    public commonService: CommonService,
    private userService: UserService,
    private router: Router,
    private fb: UntypedFormBuilder,
    @Inject(UsernameRegExp)
    private usernameRegExp: string,
    @Inject(PasswordRegExp)
    private passwordRegExp: string
  ) {
    this.registrationForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.pattern(this.usernameRegExp)]],
      password: ['', [Validators.required, Validators.pattern(this.passwordRegExp)]],
      password2: ['', [Validators.required]],
      magicKey: ['', [Validators.required]]
    }, {
      validator: MustMatch('password', 'password2')
    });
  }

  ngOnInit() {
  }

  getEmailErrorMessage() {
    if (this.registrationForm.controls.email.hasError('required')) {
      return 'Required';
    } else if (this.registrationForm.controls.email.hasError('pattern')
      || this.registrationForm.controls.email.hasError('invalid')) {
      return 'Invalid format';
    } else if (this.registrationForm.controls.email.hasError('conflict')) {
      return 'Already in use';
    }
    return '';
  }

  getUsernameErrorMessage() {
    if (this.registrationForm.controls.username.hasError('required')) {
      return 'Required';
    } else if (this.registrationForm.controls.username.hasError('pattern')
      || this.registrationForm.controls.username.hasError('invalid')) {
      return 'Invalid format';
    } else if (this.registrationForm.controls.username.hasError('conflict')) {
      return 'Already in use';
    }
    return '';
  }

  getPasswordErrorMessage() {
    if (this.registrationForm.controls.password.hasError('required')) {
      return 'Required';
    } else if (this.registrationForm.controls.password.hasError('pattern')
      || this.registrationForm.controls.password.hasError('invalid')) {
      return 'Invalid format';
    }
    return '';
  }

  getPassword2ErrorMessage() {
    if (this.registrationForm.controls.password2.hasError('required')) {
      return 'Required';
    } else if (this.registrationForm.controls.password2.hasError('pattern')) {
      return 'Invalid format';
    } else if (this.registrationForm.controls.password2.hasError('mustMatch')) {
      return 'Passwords don\'t match';
    }
    return '';
  }

  getMagicKeyErrorMessage() {
    if (this.registrationForm.controls.magicKey.hasError('required')) {
      return 'Required';
    } else if (this.registrationForm.controls.magicKey.hasError('invalid')) {
      return 'Invalid';
    }
    return '';
  }

  onSubmit() {
    if (!this.registrationForm.valid) {
      return;
    }
    const processId = this.commonService.loadingStart();
    this.userService.register(
      this.registrationForm.controls.email.value,
      this.registrationForm.controls.username.value,
      this.registrationForm.controls.password.value,
      this.registrationForm.controls.magicKey.value
    ).pipe(
      map(() => {
        this.onSuccessfulRegistration();
        return true;
      }),
      catchError((error: HttpErrorResponse) => {
        this.onFailedRegistration(error);
        return of(false);
      })
    ).subscribe(() => {
      this.commonService.loadingEnd(processId);
    });
  }

  checkRedirection() {
    this.router.navigate(['/home']);
  }

  onFailedRegistration(errorResponse: HttpErrorResponse) {
    const error = errorResponse.error as FieldErrorResponse;
    if (error != null && error.field != null && error.field !== '') {
      switch (error.field) {
        case 'magicKey':
          this.registrationForm.controls.magicKey.setErrors({ invalid: true });
          break;
        case 'Username':
          if (error.cause === FieldErrorType.CONFLICT) {
            this.registrationForm.controls.username.setErrors({ conflict: true })
          } else {
            this.registrationForm.controls.username.setErrors({ invalid: true });
          }
          break;
        case 'Email':
          if (error.cause === FieldErrorType.CONFLICT) {
            this.registrationForm.controls.email.setErrors({ conflict: true })
          } else {
            this.registrationForm.controls.email.setErrors({ invalid: true });
          }
          break;
        case 'Password':
          this.registrationForm.controls.password.setErrors({ invalid: true });
          break;
        default:
      }
    }
    this.commonService.showMessage('Registration failed');
  }

  onSuccessfulRegistration() {
    this.router.navigate(['/login']);
  }

}
