import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/services/common.service';
import { UserService } from 'src/app/services/user.service';
import { UntypedFormBuilder, Validators } from '@angular/forms';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-forgotten-password',
  templateUrl: './forgotten-password.component.html',
  styleUrls: ['./forgotten-password.component.scss', '../authentication.scss']
})
export class ForgottenPasswordComponent implements OnInit {

  forgottenPasswordForm = this.fb.group({
    email: ['', [Validators.required]]
  })

  constructor(
    public commonService: CommonService,
    private userService: UserService,
    private fb: UntypedFormBuilder
  ) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (!this.forgottenPasswordForm.valid) {
      return;
    }
    const processId = this.commonService.loadingStart();
    this.userService.forgotPassword(
      this.forgottenPasswordForm.controls.email.value
    ).pipe(
      // TODO use below solution instead of null check, for next backend update
      map(() => {
        this.onSuccessfulReset();
        return true;
      }),
      catchError(() => {
        this.onFailedReset();
        return of(false);
      })
    ).subscribe(() => {
      this.commonService.loadingEnd(processId);
    });
  }

  onSuccessfulReset(): void {
    this.forgottenPasswordForm.controls.email.reset();
    this.forgottenPasswordForm.controls.email.setErrors(null);
    this.commonService.showMessage('A reset link has been sent to your inbox');
  }

  onFailedReset(): void {
    this.commonService.showMessage('An unknown error happened');
  }

  getEmailErrorMessage() {
    if (this.forgottenPasswordForm.controls.email.hasError('required')) {
      return 'Required';
    }
    return '';
  }

}
