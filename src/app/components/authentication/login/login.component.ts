import { Component, OnInit } from '@angular/core';
import { Validators, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common.service';
import { of } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../authentication.scss']
})
export class LoginComponent implements OnInit {

  loginForm: UntypedFormGroup;

  constructor(
    public commonService: CommonService,
    private userService: UserService,
    private router: Router,
    private fb: UntypedFormBuilder
  ) {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      persistent: ['']
    });
  }

  ngOnInit() {
  }

  getUsernameErrorMessage() {
    if (this.loginForm.controls.username.hasError('required')) {
      return 'Required';
    }
    return '';
  }

  getPasswordErrorMessage() {
    if (this.loginForm.controls.password.hasError('required')) {
      return 'Required';
    }
    return '';
  }

  isFormFilled(): boolean {
    return !this.loginForm.controls.username.hasError('required') && !this.loginForm.controls.password.hasError('required');
  }

  onSubmit() {
    if (!this.isFormFilled()) {
      return;
    }
    const processId = this.commonService.loadingStart();
    this.userService.login(
      this.loginForm.controls.username.value,
      this.loginForm.controls.password.value,
      this.loginForm.controls.persistent.value === true
    ).pipe(
      // TODO use below solution instead of null check, for next backend update
      map(() => {
        this.onSuccessfulLogin();
        return true;
      }),
      catchError(() => {
        this.onFailedLogin();
        return of(false);
      })
    ).subscribe(() => {
      this.commonService.loadingEnd(processId);
    });
  }

  onFailedLogin() {
    this.loginForm.controls.username.setErrors({ invalid: true });
    this.loginForm.controls.password.setErrors({ invalid: true });
    this.commonService.showMessage('Login failed');
  }

  onSuccessfulLogin() {
    this.router.navigate(['/files']);
  }

}
