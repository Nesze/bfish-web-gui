import { Component, Inject, OnInit } from '@angular/core';
import { ApplicationVersion } from 'src/app/services/tokens';

export enum ContributionArea {
  DEVELOPER, HOSTING, SUPPORTER
}

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  members = [
    { name: 'legekka', contributionAreas: [ ContributionArea.DEVELOPER, ContributionArea.SUPPORTER, ContributionArea.HOSTING ] },
    { name: 'Nesze', contributionAreas: [ ContributionArea.DEVELOPER, ContributionArea.SUPPORTER ] },
    { name: 'Spkz', contributionAreas: [ ContributionArea.DEVELOPER, ContributionArea.SUPPORTER ] },
    { name: 'ExModify', contributionAreas: [ ContributionArea.DEVELOPER, ContributionArea.SUPPORTER ] },
    { name: 'Razeal', contributionAreas: [ ContributionArea.DEVELOPER, ContributionArea.SUPPORTER ] },
    { name: 'doughno', contributionAreas: [ ContributionArea.DEVELOPER, ContributionArea.SUPPORTER ] },
    { name: 'Sonozaki-', contributionAreas: [ ContributionArea.DEVELOPER, ContributionArea.SUPPORTER ] },
    { name: 'Sn0wPaw', contributionAreas: [ ContributionArea.SUPPORTER ] }
  ];

  constructor(
    @Inject(ApplicationVersion) public readonly appVersion: string
  ) { }

  ngOnInit(): void {
  }

  // Private helpers

  getNamesForContributionArea(contributionArea: ContributionArea): string[] {
    return this.members
      .filter(member => member.contributionAreas.includes(contributionArea))
      .map(member => member.name);
  }

  // Template helpers

  getDevelopers(): string[] {
    return this.getNamesForContributionArea(ContributionArea.DEVELOPER);
  }

  getHosts(): string[] {
    return this.getNamesForContributionArea(ContributionArea.HOSTING);
  }

  getSupporters(): string[] {
    return this.getNamesForContributionArea(ContributionArea.SUPPORTER);
  }

}
