import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FileEntry } from 'src/app/models/dto/file-entry';
import { FileType, getFileTypeByExtension } from 'src/app/models/file-type.enum';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-file-preview',
  templateUrl: './file-preview.component.html',
  styleUrls: ['./file-preview.component.scss']
})
export class FilePreviewComponent {

  readonly FileType = FileType;

  @Input()
  set file(value: FileEntry | undefined) {
    this._file = value;
    this.extension = value?.extension.split('.')[1].toUpperCase();
    this.fileName = value == null ? undefined : value.originalName + value.extension;
    this.fileType = (value == null || this.extension == null) ? undefined : getFileTypeByExtension(this.extension);
  }
  get file(): FileEntry | undefined {
    return this._file;
  }
  private _file?: FileEntry;

  @Input()
  link?: string;

  @Input('selected')
  isSelected = false;

  @Output()
  selectClick = new EventEmitter<void>();

  @Output()
  detailsClick = new EventEmitter<void>();

  extension?: string;

  fileName?: string;

  fileType?: FileType;

  constructor(
    public commonService: CommonService
  ) {}

  //#region Template event handlers

  onSelectClick(): void {
    this.selectClick.emit();
  }

  onDetailsClick(): void {
    this.detailsClick.emit();
  }

  //#endregion

}
