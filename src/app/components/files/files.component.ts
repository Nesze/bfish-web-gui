import { Component, OnInit, ViewChild, AfterViewInit, Inject, ElementRef } from '@angular/core';
import { FilesService } from 'src/app/services/files.service';
import { FileEntry } from 'src/app/models/dto/file-entry';
import { PageableList } from 'src/app/models/dto/pageable-list';
import { CommonService } from 'src/app/services/common.service';
import { of, Observable, combineLatest, BehaviorSubject } from 'rxjs';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatLegacyPaginator as MatPaginator, LegacyPageEvent as PageEvent } from '@angular/material/legacy-paginator';
import { Sort } from '@angular/material/sort';
import { switchMap, map, catchError, tap, flatMap, filter, distinctUntilChanged } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { MaxFileSize } from 'src/app/services/tokens';
import { UserService } from 'src/app/services/user.service';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { ConfirmDialogComponent, ConfirmDialogData } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { HttpEventType } from '@angular/common/http';
import { FileSizePipe } from 'ngx-filesize';
import { FileDetailsDialogComponent, FileDetailsDialogData, FileDetailsDialogResult } from '../dialogs/file-details-dialog/file-details-dialog.component';
import { FileEntryFilter } from 'src/app/models/dto/file-entry-filter';
import { SortingOption, SortingOptionGroup } from 'src/app/models/sorting-option';
import { debounceTimeAfterFirst } from 'src/app/helpers/debounce-time-after';
import { DefaultFileSortingOption, FileSortingOptions } from 'src/app/models/file-sorting-options';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.scss']
})
export class FilesComponent implements OnInit, AfterViewInit {

  @ViewChild('fileUpload') 
  fileInput?: ElementRef;
  isUploading: boolean = false;
  uploadProgress?: number;

  @ViewChild(MatPaginator, { static: true })
  paginator?: MatPaginator;

  filter: UntypedFormGroup;
  displayedColumns: string[] = ['select', 'name', 'uploadDate', 'size', 'actions'];
  displayedColumnsMobile: string[] = ['select', 'name', 'actions'];

  pageSize: number = 25;
  pageSizeOptions: number[] = [10, 25, 50, 100];

  sortingOptions: SortingOptionGroup[] = FileSortingOptions;

  // Data source
  fileList: PageableList<FileEntry> = { items: [], totalItems: 0 };
  errorLoadingFiles: boolean = false;

  // Selection
  selection = new SelectionModel<FileEntry>(true, []);
  selectionDisplay?: string;

  sort$ = new BehaviorSubject<Sort>(DefaultFileSortingOption);
  paging$ = new BehaviorSubject<PageEvent>({ pageIndex: 0, pageSize: this.pageSize, length: 0 });
  filterValue$ = new BehaviorSubject<FileEntryFilter>({});
  reloadSource$ = new BehaviorSubject<void>(undefined);

  userBaseUrl?: string;

  constructor(
    public commonService: CommonService,
    private userService: UserService,
    private filesService: FilesService,
    private fb: UntypedFormBuilder,
    private dialog: MatDialog,
    private fileSizePipe: FileSizePipe,
    @Inject(MaxFileSize) private maxFileSize: number
  ) {
    this.filter = this.fb.group({ fullName: [''] });
  }

  ngOnInit(): void {
    this.initUserBaseUrl();
    this.initControls();
    this.initSelectionDisplay();
  }

  ngAfterViewInit(): void {
    this.filter.valueChanges.subscribe(value => this.onFilterValueChange(value));
  }

  //#region Initializers

  private initUserBaseUrl(): void {
    this.userService.getUserBaseUrl().subscribe(url => this.userBaseUrl = url);
  }

  private initControls(): void {
    combineLatest([
      this.sort$.pipe(distinctUntilChanged()),
      this.paging$,
      this.filterValue$.pipe(debounceTimeAfterFirst(500)),
      this.reloadSource$
    ]).pipe(
      flatMap(([sortBy, paging, filter]: [Sort, PageEvent, FileEntryFilter, void]) => {
        return this.updateDataSource(
          paging.pageIndex,
          paging.pageSize,
          filter,
          sortBy
        );
      }),
      tap(() => this.selection.clear())
    ).subscribe();
  }

  private initSelectionDisplay(): void {
    this.selection.changed.subscribe(event => {
      const selectionCount = event.source.selected.length;
      const selectionFileSize = event.source.selected.reduce((sum, file) => sum += file.sizeInBytes, 0);
      if (selectionCount === 0) {
        this.selectionDisplay = '';
      } else if (selectionCount === 1) {
        this.selectionDisplay = `1 file selected (${this.fileSizePipe.transform(selectionFileSize)})`;
      } else {
        this.selectionDisplay = `${selectionCount} files selected (${this.fileSizePipe.transform(selectionFileSize)})`;
      }
    });
  }

  //#endregion

  //#region Private helpers

  private updateDataSource(
    pageIndex: number, pageSize: number, filter: FileEntryFilter, sortBy: Sort
  ): Observable<void> {
    const processId = this.commonService.loadingStart();
    return this.filesService.getFileEntries(pageIndex, pageSize, filter, sortBy).pipe(
      tap(() => this.errorLoadingFiles = false),
      catchError(() => {
        this.errorLoadingFiles = true;
        return of({ items: [], totalItems: 0 } as PageableList<FileEntry>);
      }),
      map(fileList => {
        this.commonService.loadingEnd(processId);
        this.fileList = fileList;
      })
    );
  }

  private promptUser(title: string, message: string[], items?: string[]): Observable<boolean> {
    return this.dialog.open<ConfirmDialogComponent, ConfirmDialogData>(ConfirmDialogComponent, {
      width: '512px',
      data: { title, message, items }
    }).afterClosed().pipe(
      map(value => value != null)
    );
  }

  private uploadingStart(): void {
    this.isUploading = true;
  }

  private uploadingEnd(): void {
    this.isUploading = false;
  }

  private resetFileInput(): void {
    if (this.fileInput != null) {
      this.fileInput.nativeElement.value = '';
    }
  }

  //#endregion

  //#region Template helpers

  getDisplayedColumns(): Observable<String[]> {
    return this.commonService.isHandset$.pipe(
      map(isHandset => isHandset ? this.displayedColumnsMobile : this.displayedColumns)
    );
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.fileList ? this.fileList.items.length : 0;
    return numRows > 0 && numSelected === numRows;
  }

  checkboxLabel(row?: FileEntry): string {
    if (row == null) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.originalName + row.extension}`;
  }

  showUnderConstructionMessage(): void {
    this.commonService.showMessage('This feature is under construction. Check back later!');
  }

  generateLink(file: FileEntry): string {
    return `${this.userBaseUrl}/${file.accessName + file.extension}`;
  }

  generateLinkWithoutExtension(file: FileEntry): string {
    return `${this.userBaseUrl}/${file.accessName}`;
  }

  uploadFile(files: FileList): void {
    if (files.length == 0) {
      // this.commonService.showMessage('No file has been chosen');
      return;
    }
    const file = files[0];
    if (file.size > this.maxFileSize) {
      this.commonService.showMessage(`The maximum filesize is ${this.fileSizePipe.transform(this.maxFileSize)}`);
      return;
    }
    this.uploadingStart();
    this.filesService.uploadFile(file).pipe(
      filter(event => {
        if (event.type === HttpEventType.UploadProgress && event.total != null) {
          this.uploadProgress = Math.floor(100 * event.loaded / event.total);
          return false;
        } else {
          this.uploadProgress = undefined;
        }
        return event.type === HttpEventType.Response;
      })
    ).subscribe(event => {
      this.uploadingEnd();
      this.resetFileInput();
      this.commonService.showMessage(`${file.name} uploaded`);
      this.reloadSource$.next();
    }, () => {
      this.uploadingEnd();
      this.resetFileInput();
      this.commonService.showMessage(`Uploading ${file.name} failed`);
    });
  }

  //#endregion

  //#region Template event handlers

  onSelect(file: FileEntry): void {
    this.selection.toggle(file);
  }

  onSelectAllClick(): void {
    this.fileList.items.forEach(row => this.selection.select(row));
  }

  onClearSelectionClick(): void {
    this.selection.clear();
  }

  onSortChange(sortOption: SortingOption): void {
    this.sort$.next(sortOption.value);
  }

  onPaginatorValueChange(value: PageEvent): void {
    this.paging$.next(value);
  }

  onFilterValueChange(value: FileEntryFilter): void {
    this.filterValue$.next(value);
  }

  onDeleteSelectionButtonClick(): void {
    const selectionCount = this.selection.selected.length;
    const promptTitle = selectionCount === 1
      ? 'Delete file'
      : 'Delete multiple files';
    const promptMessage = selectionCount === 1
      ? `Are you sure you want to delete the following file?`
      : `Are you sure you want to delete the following ${selectionCount} files?`;
    this.promptUser(
      promptTitle, 
      [ promptMessage ],
      this.selection.selected.map(file => file.originalName + file.extension)
    ).pipe(
      filter(confirmed => confirmed),
      switchMap(() => this.filesService.deleteFiles(this.selection.selected))
    ).subscribe(() => {
      this.commonService.showMessage(`${selectionCount} files deleted successfully`);
      this.reloadSource$.next();
    }, () => {
      this.commonService.showMessage('Error deleting files');
    });
  }

  onDetailsButtonClick(row: FileEntry): void {
    const data: FileDetailsDialogData = {
      file: row,
      link: this.generateLink(row)
    };
    this.dialog.open<FileDetailsDialogComponent, FileDetailsDialogData, FileDetailsDialogResult>(
      FileDetailsDialogComponent, 
      { width: '384px', data }
    ).afterClosed().pipe(
      filter(result => result != null && result.isDeleted)
    ).subscribe(() => {
      this.reloadSource$.next();
    });
  }

  onRefreshButtonClick(): void {
    this.reloadSource$.next();
  }

  onFilesDrop(files: FileList) {
    this.uploadFile(files);
  }

  onUploadFileChosen(files: FileList | null): void {
    if (files == null) {
      return;
    }
    
    this.uploadFile(files);
  }

  //#endregion

}
