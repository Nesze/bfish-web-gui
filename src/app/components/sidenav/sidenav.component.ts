import { Component, ViewChild, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { CommonService } from 'src/app/services/common.service';
import { DisplayFor } from 'src/app/models/menu-item';
import { UserService } from 'src/app/services/user.service';
import { UserDetails } from 'src/app/models/dto/user-details';
import { filter, take, catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of } from 'rxjs';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  @ViewChild(MatSidenav)
  sidenav?: MatSidenav;

  menuItems = [
    {
      title: 'My Files',
      icon: 'folder',
      path: '/files',
      displayFor: DisplayFor.USER
    },
    {
      title: 'Profile',
      icon: 'account_circle',
      path: '/profile',
      displayFor: DisplayFor.USER
    },
    {
      title: 'About',
      icon: 'info',
      path: '/about',
      displayFor: DisplayFor.BOTH
    }
  ];

  user?: UserDetails;

  isHandset?: boolean;

  constructor(
    public commonService: CommonService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initUser();
    this.commonService.isHandset$.subscribe(isHandset => this.isHandset = isHandset);
  }

  // Initializers

  private initUser() {
    const processId = this.commonService.loadingStart();
    this.userService.user$.subscribe(user => {
      this.user = user ?? undefined;
      if (this.commonService.isProcessLoading(processId)) {
        this.commonService.loadingEnd(processId);
      }
    });
  }

  // Template event handlers

  onMenuButtonClick() {
    this.sidenav?.toggle();
  }

  onMenuItemClick() {
    if (this.isHandset) {
      this.sidenav?.close();
    }
  }

  onLogoutButtonClick() {
    const processId = this.commonService.loadingStart();
    this.userService.logout().pipe(
      map(() => true),
      catchError(() => of(false))
    ).subscribe(isLogoutSuccessful => {
      this.commonService.loadingEnd(processId);
      if (isLogoutSuccessful) {
        this.router.navigate(['/login']);
      } else {
        this.commonService.showMessage('Couldn\'t log out');
      }
    });
  }

}
