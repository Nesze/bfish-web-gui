import { Component, OnInit, Inject } from '@angular/core';
import { FileEntry } from 'src/app/models/dto/file-entry';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef, MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { FilesService } from 'src/app/services/files.service';
import { saveAs } from 'file-saver';
import { CommonService } from 'src/app/services/common.service';
import { Clipboard } from '@angular/cdk/clipboard';
import { Observable } from 'rxjs';
import { ConfirmDialogComponent, ConfirmDialogData } from '../confirm-dialog/confirm-dialog.component';
import { filter, map, switchMap } from 'rxjs/operators';

export interface FileDetailsDialogData {
  file: FileEntry;
  link: string;
}

export interface FileDetailsDialogResult {
  isDeleted: boolean;
}

@Component({
  selector: 'app-file-details-dialog',
  templateUrl: './file-details-dialog.component.html',
  styleUrls: ['./file-details-dialog.component.scss']
})
export class FileDetailsDialogComponent implements OnInit {

  readonly fileName: string;

  constructor(
    private readonly filesService: FilesService,
    public readonly commonService: CommonService,
    private clipboard: Clipboard,
    private dialog: MatDialog,
    private readonly dialogRef: MatDialogRef<FileDetailsDialogComponent, FileDetailsDialogResult>,
    @Inject(MAT_DIALOG_DATA)
    public readonly data: FileDetailsDialogData
  ) {
    this.fileName = data.file.originalName + data.file.extension;
  }

  ngOnInit() {
  }

  //#region Private helpers

  private promptUser(title: string, message: string[], items?: string[]): Observable<boolean> {
    return this.dialog.open<ConfirmDialogComponent, ConfirmDialogData>(ConfirmDialogComponent, {
      width: '512px',
      data: { title, message, items }
    }).afterClosed().pipe(
      map(value => value != null)
    );
  }

  //#endregion

  //#region Template event handlers

  onDeleteClick(): void {
    this.promptUser(
      'Delete file', 
      [ `Are you sure you want to delete the following file?` ], 
      [ this.fileName ]
    ).pipe(
      filter(confirmed => confirmed),
      switchMap(() => this.filesService.deleteFile(this.data.file))
    ).subscribe(() => {
      this.commonService.showMessage(`${this.fileName} deleted successfully`);
      this.dialogRef.close({ isDeleted: true });
    }, () => {
      this.commonService.showMessage('Error deleting file');
    });
  }

  onCopyLinkClick(): void {
    this.clipboard.copy(this.data.link);
    this.commonService.showMessage('Link copied to clipboard');
  }

  onShareClick(): void {
    if (navigator.share) {
      navigator.share({
        title: `${this.fileName} ${this.data.link}`
      }).catch(e => {
        this.commonService.showMessage('An error happened while sharing the link');
        console.log(e);
      });
    } else {
      this.commonService.showMessage('Sharing is not available in this browser');
    }
  }

  onDownloadClick(): void {
    this.filesService.getFile(this.data.file).subscribe(
      downloadFile => saveAs(downloadFile.blob, downloadFile.fileName)
    );
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  //#endregion

}
