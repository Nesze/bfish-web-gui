import { Component, OnInit, Input } from '@angular/core';
import { Validators, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { UserService } from 'src/app/services/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { FieldErrorResponse } from 'src/app/models/dto/field-error-response';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent implements OnInit {

  @Input()
  currentEmail?: string;

  form: UntypedFormGroup;

  constructor(
    public commonService: CommonService,
    private userService: UserService,
    private fb: UntypedFormBuilder
  ) {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  ngOnInit(): void {
  }

  // Private helpers

  private resetForm(): void {
    this.form.reset();
    this.form.controls.email.setErrors(null);
  }

  // Private event handlers

  onSuccessfulEmailChange(): void {
    this.resetForm();
    this.commonService.showMessage('Email address changed successfully');
  }

  onFailedEmailChange(errorResponse: HttpErrorResponse): void {
    const error = errorResponse.error as FieldErrorResponse;
    if (error != null && error.field != null && error.field !== '') {
      if (error.field === 'email') {
        this.form.controls.email.setErrors({ invalid: true });
      }
    }
    this.commonService.showMessage('Email change failed');
  }

  // Template helpers

  getEmailErrorMessage() {
    if (this.form.controls.email.hasError('required')) {
      return 'Required';
    } else if (this.form.controls.email.hasError('pattern')
      || this.form.controls.email.hasError('invalid')) {
      return 'Invalid format';
    } else if (this.form.controls.email.hasError('conflict')) {
      return 'Already in use';
    }
    return '';
  }

  onSubmit(): void {
    if (!this.form.valid) {
      return;
    }
    const processId = this.commonService.loadingStart();
    this.userService.changeEmail(this.form.controls.email.value).pipe(
      map(() => {
        this.onSuccessfulEmailChange();
        return true;
      }),
      catchError((error: HttpErrorResponse) => {
        this.onFailedEmailChange(error);
        return of(false);
      })
    ).subscribe(() => {
      this.commonService.loadingEnd(processId);
    });
  }

}
