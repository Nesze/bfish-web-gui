import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { CommonService } from 'src/app/services/common.service';
import { UserDetails } from 'src/app/models/dto/user-details';
import { Clipboard } from '@angular/cdk/clipboard';
import { filter, flatMap, switchMap, map, catchError, tap } from 'rxjs/operators';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { ConfirmDialogComponent, ConfirmDialogData } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { of } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user?: UserDetails;
  sharexConfig?: string;
  sharexConfigHtml?: string;

  constructor(
    private userService: UserService,
    private commonService: CommonService,
    private clipboard: Clipboard,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.userService.user$.pipe(
      tap(user => this.user = user ?? undefined)
    ).subscribe(() => {
      this.initShareXConfig();
    });
  }

  // Initializers

  initShareXConfig(): void {
    if (this.user == null) {
      this.sharexConfig = 'Error loading config';
      this.sharexConfigHtml = 'Error loading config';
      return;
    }

    const config = {
      "Version": "12.4.1",
      "Name": "BFish",
      "DestinationType": "FileUploader",
      "RequestMethod": "POST",
      "RequestURL": "https://fs.boltz.hu/api/files/upload",
      "Body": "MultipartFormData",
      "Arguments": {
        "key": this.user.apiKey
      },
      "FileFormName": "file",
      "URL": "$json:url$",
      "DeletionURL": "$json:deletionURL$"
    };
    this.sharexConfig = JSON.stringify(config, null, 2);
    this.sharexConfigHtml = this.sharexConfig
      .replace(new RegExp('\n', 'g'), "<br>")
      .replace(new RegExp(' ', 'g'), "&nbsp;");
  }

  // Private event handlers

  onResetApiKeySuccessful(): void {
    this.commonService.showMessage('API key reset successfully');
  }

  onResetApiKeyFailed(): void {
    this.commonService.showMessage('API key reset failed');
  }

  // Template event handlers

  onResetApiKeyButtonClick(): void {
    let processId: string;
    this.dialog.open<ConfirmDialogComponent, ConfirmDialogData>(ConfirmDialogComponent, {
      width: '512px',
      data: { 
        title: 'Reset API key',
        message: ['Are you sure you want to reset your API key?', 'This action cannot be undone.'] 
      }
    }).afterClosed().pipe(
      filter(confirmed => confirmed),
      tap(() => processId = this.commonService.loadingStart()),
      switchMap(() => this.userService.resetApiKey()),
      map(() => {
        this.onResetApiKeySuccessful();
        return true;
      }),
      catchError(() => {
        this.onResetApiKeyFailed();
        return of(false);
      })
    ).subscribe(() => {
      this.commonService.loadingEnd(processId);
    });
  }

  onCopyButtonClick(value: string) {
    this.clipboard.copy(value);
    this.commonService.showMessage('Copied to clipboard');
  }

}
