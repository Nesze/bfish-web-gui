import { Component, OnInit, Inject } from '@angular/core';
import { MustMatch } from 'src/app/utils/must-match';
import { Validators, UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { PasswordRegExp } from 'src/app/services/tokens';
import { CommonService } from 'src/app/services/common.service';
import { UserService } from 'src/app/services/user.service';
import { map, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { FieldErrorResponse } from 'src/app/models/dto/field-error-response';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  form: UntypedFormGroup;

  constructor(
    public commonService: CommonService,
    private userService: UserService,
    private fb: UntypedFormBuilder,
    @Inject(PasswordRegExp) private passwordRegExp: string
  ) {
    this.form = this.fb.group({
      password: ['', [Validators.required]],
      newPassword: ['', [Validators.required, Validators.pattern(this.passwordRegExp)]],
      newPassword2: ['', [Validators.required]]
    }, {
      validator: MustMatch('newPassword', 'newPassword2')
    });
  }

  ngOnInit(): void {
  }

  // Private helpers

  private resetForm(): void {
    this.form.reset();
    this.form.controls.password.setErrors(null);
    this.form.controls.newPassword.setErrors(null);
    this.form.controls.newPassword2.setErrors(null);
  }

  // Private event handlers

  onSuccessfulPasswordChange(): void {
    this.resetForm();
    this.commonService.showMessage('Password changed successfully');
  }

  onFailedPasswordChange(errorResponse: HttpErrorResponse): void {
    const error = errorResponse.error as FieldErrorResponse;
    if (error != null && error.field != null && error.field !== '') {
      switch (error.field) {
        case 'password':
          this.form.controls.password.setErrors({ invalid: true });
          break;
        case 'newPassword':
          this.form.controls.newPassword.setErrors({ invalid: true });
          break;
      }
    }
    this.commonService.showMessage('Password change failed');
  }

  // Template helpers

  getPasswordErrorMessage() {
    if (this.form.controls.password.hasError('required')) {
      return 'Required';
    } else if (this.form.controls.password.hasError('invalid')) {
      return 'Invalid password';
    }
    return '';
  }

  getNewPasswordErrorMessage() {
    if (this.form.controls.newPassword.hasError('required')) {
      return 'Required';
    } else if (this.form.controls.newPassword.hasError('pattern')
      || this.form.controls.newPassword.hasError('invalid')) {
      return 'Invalid format';
    }
    return '';
  }

  getNewPassword2ErrorMessage() {
    if (this.form.controls.newPassword2.hasError('required')) {
      return 'Required';
    } else if (this.form.controls.newPassword2.hasError('pattern')) {
      return 'Invalid format';
    } else if (this.form.controls.newPassword2.hasError('mustMatch')) {
      return 'Passwords don\'t match';
    }
    return '';
  }

  // Template event handlers

  onSubmit(): void {
    if (!this.form.valid) {
      return;
    }
    const processId = this.commonService.loadingStart();
    this.userService.changePassword(
      this.form.controls.newPassword.value,
      this.form.controls.password.value
    ).pipe(
      map(() => {
        this.onSuccessfulPasswordChange();
        return true;
      }),
      catchError((error: HttpErrorResponse) => {
        this.onFailedPasswordChange(error);
        return of(false);
      })
    ).subscribe(() => {
      this.commonService.loadingEnd(processId);
    });
  }

}
