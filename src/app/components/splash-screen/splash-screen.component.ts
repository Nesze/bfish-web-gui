import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.component.html',
  styleUrls: ['./splash-screen.component.scss']
})
export class SplashScreenComponent implements OnInit {

  @Input()
  visible?: boolean;

  @Input()
  error?: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

}
