import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

import { filter, map, tap, delay, debounceTime } from 'rxjs/operators'
import { CommonService } from './services/common.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private commonService: CommonService,
    private userService: UserService
  ) { }

  isUserLoading = true;
  errorLoadingUser = false;
  
  splashScreenVisible: boolean = true;

  ngOnInit() {
    this.initAutomaticTitleUpdate();
    this.userService.isUserLoading().pipe(
      filter(isUserLoading => !isUserLoading),
      debounceTime(500),
      tap(isUserLoading => this.isUserLoading = isUserLoading),
      delay(250)
    ).subscribe({
      next: () => {
        this.splashScreenVisible = false;
      },
      error: () => {
        this.errorLoadingUser = true;
      }
    });
  }

  initAutomaticTitleUpdate() {
    const appTitle = this.titleService.getTitle();
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => {
        let child = this.activatedRoute.firstChild;
        while (child != null && child.firstChild != null) {
          child = child.firstChild;
        }
        if (child != null && child.snapshot.data.title) {
          return child.snapshot.data.title;
        }
        return appTitle;
      })
    ).subscribe((title: string) => {
      this.commonService.updatePageTitle(title);
      this.titleService.setTitle(`${title} | Bfish`);
    });
  }

}
