import { Injectable } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, ReplaySubject, BehaviorSubject } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private loadingProcesses: string[] = [];

  readonly isHandset$: Observable<boolean>;

  private pageTitleSource$: ReplaySubject<string> = new ReplaySubject(1);
  private pageTitle$: Observable<string> = this.pageTitleSource$.asObservable();
  public get pageTitle() {
    return this.pageTitle$;
  }

  private isLoadingSource$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private isLoading$: Observable<boolean> = this.isLoadingSource$.asObservable().pipe(debounceTime(500));
  public get isLoading() {
    return this.isLoading$;
  }

  constructor(
    private breakpointObserver: BreakpointObserver,
    private snackBar: MatSnackBar
  ) {
    this.isHandset$ = this.breakpointObserver.observe(Breakpoints.Handset).pipe(map(result => result.matches));
  }

  // Private helpers

  private processesLoading(): boolean {
    return this.loadingProcesses.length > 0;
  }

  // Public methods

  updatePageTitle(title: string): void {
    this.pageTitleSource$.next(title);
  }

  loadingStart(): string {
    const processUuid = uuidv4();
    this.loadingProcesses.push(processUuid);

    this.isLoadingSource$.next(true);
    return processUuid;
  }

  isProcessLoading(processId: string): boolean {
    return this.loadingProcesses.includes(processId);
  }

  loadingEnd(processId: string): void {
    const processIdIndex = this.loadingProcesses.findIndex(id => id === processId);
    if (processIdIndex === -1) {
      throw new Error('Process ID not found');
    }
    this.loadingProcesses.splice(processIdIndex, 1);
    this.isLoadingSource$.next(this.processesLoading());
  }

  showMessage(msg: string): void {
    this.snackBar.open(msg, 'Close', {
      duration: 5000
    });
  }

}
