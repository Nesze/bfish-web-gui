import { Injectable, Inject } from '@angular/core';
import { FilesServiceBaseUrl } from './tokens';
import { HttpClient, HttpParams, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PageableList } from '../models/dto/pageable-list';
import { FileEntry } from '../models/dto/file-entry';
import { map } from 'rxjs/operators';
import { FileEntryFilter } from '../models/dto/file-entry-filter';
import { Sort } from '@angular/material/sort';
import { UserDetails } from '../models/dto/user-details';
import { UserService } from './user.service';
import { DownloadFile } from '../models/download-file';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  private user?: UserDetails;

  constructor(
    @Inject(FilesServiceBaseUrl) private baseUrl: string,
    private http: HttpClient,
    private userService: UserService
  ) {
    userService.user$.subscribe(user => this.user = user ?? undefined);
  }

  getFileEntries(page: number, pageSize: number, filter: FileEntryFilter, sort: Sort): Observable<PageableList<FileEntry>> {
    let params = new HttpParams();

    if (page != null) {
      params = params.set('page', page);
    }
    if (pageSize != null) {
      params = params.set('pageSize', pageSize);
    }
    if (sort != null && sort.direction !== '' && sort.active !== 'created') {
      params = params.set('sortBy', `${sort.active}:${sort.direction}`);
    }
    if (filter != null) {
      if (filter.name != null) {
        params = params.set('name', filter.name);
      }
      if (filter.extension != null) {
        params = params.set('extension', filter.extension);
      }
      if (filter.fullName != null) {
        params = params.set('fullName', filter.fullName);
      }
      if (filter.uploadDateFrom != null) {
        params = params.set('uploadDateFrom', filter.uploadDateFrom.toISOString());
      }
      if (filter.uploadDateTo != null) {
        params = params.set('uploadDateTo', filter.uploadDateTo.toISOString());
      }
      if (filter.minSize != null) {
        params = params.set('minSize', filter.minSize);
      }
      if (filter.maxSize != null) {
        params = params.set('maxSize', filter.maxSize);
      }
    }

    return this.http.get<PageableList<FileEntry>>(`${this.baseUrl}`, {
      params: params,
      withCredentials: true
    });
  }

  deleteFile(file: FileEntry): Observable<void> {
    return this.http.delete<void>(`${this.baseUrl}/${file.accessName}`, {
      withCredentials: true
    });
  }

  deleteFiles(files: FileEntry[]): Observable<void> {
    return this.http.delete<void>(this.baseUrl, {
      params: { fileNames: files.map(file => file.accessName) },
      withCredentials: true
    });
  }

  getFile(file: FileEntry): Observable<DownloadFile> {
    return this.http.get(`${this.baseUrl}/${file.accessName}`, {
      responseType: 'blob',
      withCredentials: true
    }).pipe(
      map(blob => ({
          blob: blob,
          fileName: file.originalName
        } as DownloadFile)
      )
    );
  }

  uploadFile(file: File): Observable<HttpEvent<Object>> {
    if (this.user == null) {
      throw new Error('User is not logged in');
    }
    if (this.user.apiKey == null) {
      throw new Error('User does not have a valid API key')
    }
    const formData = new FormData();
    formData.append('file', file);
    formData.append('key', this.user.apiKey);
    return this.http.post(`${this.baseUrl}/upload`, formData, {
      reportProgress: true,
      observe: 'events'
    });
  }
}
