import { InjectionToken } from '@angular/core';

export const ApplicationVersion = new InjectionToken('ApplicationVersion');

export const UserServiceBaseUrl = new InjectionToken('UserServiceBaseUrl');
export const FilesServiceBaseUrl = new InjectionToken('FilesServiceBaseUrl');

export const UserBaseUrl = new InjectionToken('UserBaseUrl');

export const UsernameRegExp = new InjectionToken('UsernameRegExp');
export const PasswordRegExp = new InjectionToken('PasswordRegExp');

export const MaxFileSize = new InjectionToken('MaxFileSize');
