import { Injectable, Inject } from '@angular/core';
import { UserServiceBaseUrl, UserBaseUrl } from './tokens';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { UserDetails } from '../models/dto/user-details';
import { Observable, Subject, of, ReplaySubject, BehaviorSubject, throwError } from 'rxjs';
import { switchMap, catchError, share, take, map, filter, flatMap, tap, delay } from 'rxjs/operators';
import { RegistrationRequest } from '../models/dto/registration-request';
import { LoginRequest } from '../models/dto/login-request';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _userSource$: ReplaySubject<UserDetails | null> = new ReplaySubject(1);
  private isUserLoading$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  readonly user$ = this._userSource$.asObservable();

  constructor(
    @Inject(UserServiceBaseUrl) private baseUrl: string,
    @Inject(UserBaseUrl) private userBaseUrl: string,
    private http: HttpClient
  ) {
    this.fetchUser().subscribe(user => {
        this.updateUser(user);
    });
  }

  // Private helpers

  private updateUser(user: UserDetails | null): void {
    this._userSource$.next(user);
  }

  private fetchUser(): Observable<UserDetails | null> {
    this.userLoadingStart();
    return this.http.get<UserDetails>(`${this.baseUrl}/details`, {
      withCredentials: true
    }).pipe(
      tap(() => this.userLoadingEnd()),
      catchError(error => {
        if (error instanceof HttpErrorResponse && error.status === 0) {
          this.userLoadingEnd(error);
        }
        return of(null);
      }),
    );
  }

  private userLoadingStart(): void {
    this.isUserLoading$.next(true);
  }

  private userLoadingEnd(error?: any): void {
    if (error != null) {
      this.isUserLoading$.error(error);
      return;
    }
    
    this.isUserLoading$.next(false);
  }

  // Public functions

  getUserBaseUrl(): Observable<string> {
    return this.user$.pipe(
      filter((user): user is UserDetails => user != null),
      map(user => this.userBaseUrl.replace('<user>', (user.username ?? '').toLowerCase()))
    );
  }

  isUserLoading(): Observable<boolean> {
    return this.isUserLoading$.asObservable();
  }

  login(username: string, password: string, persistent: boolean): Observable<UserDetails> {
    return this.http.post<UserDetails>(`${this.baseUrl}/login`,
      { username: username, password: password, persistent: persistent } as LoginRequest,
      { withCredentials: true }
    ).pipe(
      tap(user => this.updateUser(user))
    );
  }

  register(email: string, username: string, password: string, magicKey: string): Observable<void> {
    return this.http.post<void>(`${this.baseUrl}/register`,
      { email: email, username: username, password: password } as RegistrationRequest,
      { params: { magickey: magicKey }, withCredentials: true }
    );
  }

  logout(): Observable<void> {
    return this.http.get<void>(`${this.baseUrl}/logout`,
      { withCredentials: true }
    ).pipe(
      tap(() => this.updateUser(null))
    );
  }

  resetApiKey(): Observable<UserDetails> {
    return this.http.post<UserDetails>(`${this.baseUrl}/reset-key`, null,
      { withCredentials: true }
    ).pipe(
      tap(user => this.updateUser(user))
    );
  }

  changePassword(newPassword: string, password: string): Observable<void> {
    return this.http.post<void>(`${this.baseUrl}/change-password`, null,
      { params: { newPassword, password }, withCredentials: true }
    );
  }

  changeEmail(newEmail: string): Observable<UserDetails> {
    return this.http.post<UserDetails>(`${this.baseUrl}/change-email`, null,
      { params: { newEmail }, withCredentials: true }
    ).pipe(
      tap(user => this.updateUser(user))
    );
  }

  forgotPassword(email: string): Observable<void> {
    return this.http.post<void>(`${this.baseUrl}/forgotten-password`, null,
      { params: { email } }
    );
  }

  resetPassword(email: string, password: string, token: string): Observable<void> {
    return this.http.post<void>(`${this.baseUrl}/reset-password`, null,
      { params: { email, token, password } }
    );
  }

}
