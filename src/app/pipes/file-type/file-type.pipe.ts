import { Pipe, PipeTransform } from '@angular/core';
import { FileType, getFileTypeByExtension } from 'src/app/models/file-type.enum';

@Pipe({
  name: 'fileType'
})
export class FileTypePipe implements PipeTransform {

  transform(extension?: string): FileType {
    return getFileTypeByExtension(extension);
  }

}
