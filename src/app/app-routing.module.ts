import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/authentication/login/login.component';
import { LoginActivate } from './utils/login-activate';
import { RegistrationComponent } from './components/authentication/registration/registration.component';
import { FilesComponent } from './components/files/files.component';
import { ProfileComponent } from './components/profile/profile.component';
import { Role } from './models/role';
import { ForgottenPasswordComponent } from './components/authentication/forgotten-password/forgotten-password.component';
import { AboutComponent } from './components/about/about.component';
import { ResetPasswordComponent } from './components/authentication/reset-password/reset-password.component';

const routes: Routes = [
  {
    path: '',
    component: SidenavComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'files'
      },
      {
        path: 'home',
        component: HomeComponent,
        canActivate: [LoginActivate],
        data: {
          title: "Home",
          allowedRoles: [ Role.USER, Role.GUEST ]
        }
      },
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [LoginActivate],
        data: {
          title: 'Login',
          allowedRoles: [ Role.GUEST ]
        }
      },
      {
        path: 'register',
        component: RegistrationComponent,
        canActivate: [LoginActivate],
        data: {
          title: 'Registration',
          allowedRoles: [ Role.GUEST ]
        }
      },
      {
        path: 'forgotten-password',
        component: ForgottenPasswordComponent,
        canActivate: [LoginActivate],
        data: {
          title: 'Forgot password',
          allowedRoles: [ Role.GUEST ]
        }
      },
      {
        path: 'reset-password',
        component: ResetPasswordComponent,
        canActivate: [LoginActivate],
        data: {
          title: 'Reset password',
          allowedRoles: [ Role.GUEST ]
        }
      },
      {
        path: 'files',
        component: FilesComponent,
        canActivate: [LoginActivate],
        data: {
          title: 'Files',
          allowedRoles: [ Role.USER ]
        }
      },
      {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [LoginActivate],
        data: {
          title: 'Profile',
          allowedRoles: [ Role.USER ]
        }
      },
      {
        path: 'about',
        component: AboutComponent,
        data: {
          title: 'About',
          allowedRoles: [ Role.USER, Role.GUEST ]
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
