import { SortingOptionGroup } from "./sorting-option";

export const FileSortingOptions: SortingOptionGroup[] = [
  {
    displayName: 'Sort by name',
    icon: 'sort_by_alpha',
    children: [
      {
        displayName: 'A to Z',
        value: { active: 'name', direction: 'asc' }
      },
      {
        displayName: 'Z to A',
        value: { active: 'name', direction: 'desc' }
      }
    ]
  },
  {
    displayName: 'Sort by size',
    icon: 'storage',
    children: [
      {
        displayName: 'Smallest first',
        value: { active: 'size', direction: 'asc' }
      },
      {
        displayName: 'Largest first',
        value: { active: 'size', direction: 'desc' }
      }
    ]
  },
  {
    displayName: 'Sort by date',
    icon: 'today',
    children: [
      {
        displayName: 'Oldest first',
        value: { active: 'uploadDate', direction: 'asc' }
      },
      {
        displayName: 'Newest first',
        value: { active: 'uploadDate', direction: 'desc' }
      }
    ]
  }
];

export const DefaultFileSortingOption = FileSortingOptions[2].children[1].value;