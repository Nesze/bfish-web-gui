export interface Action {
  displayName: string;
  iconName: string;
  execute: (data: any) => void;
  disabled?: boolean;
}
