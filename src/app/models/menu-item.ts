export interface MenuItem {
  title: string;
  icon?: string;
  path: string;
  displayFor: DisplayFor;
}

export enum DisplayFor {
  USER,
  GUEST,
  BOTH
}
