import { MatPaginatorIntl } from "@angular/material/paginator";

export class MatPaginatorCustomIntl extends MatPaginatorIntl {

  itemsPerPageLabel = 'Page size';
  
  getRangeLabel = (page: number, pageSize: number, length: number) => {
    const pageCount = Math.ceil(length / pageSize);
    return `${page + 1} / ${pageCount}`;
  }

}