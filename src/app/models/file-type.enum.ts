import * as mime from "mime";

export enum FileType {
  Audio = 'audio',
  Video = 'video',
  Image = 'image',
  Text = 'text',
  Document = 'document',
  Archive = 'archive',
  Font = 'font',
  Other = 'other'
}

export function getFileTypeByExtension(extension?: string): FileType {
  if (extension == null) {
    return FileType.Other;
  }
  const mimeType = mime.getType(extension);
  if (mimeType == null) {
    return FileType.Other;
  }
  const type = mimeType.split('/')[0];
  if (type === 'image') {
    return FileType.Image;
  }
  if (type === 'audio') {
    return FileType.Audio;
  }
  if (type === 'video') {
    return FileType.Video;
  }
  if (type === 'text' || mimeType === 'application/json') {
    return FileType.Text;
  }
  if (type === 'font') {
    return FileType.Font;
  }
  if ([
    'application/zip',
    'application/x-7z-compressed',
    'application/x-freearc',
    'application/x-bzip',
    'application/x-bzip2',
    'application/gzip',
    'application/java-archive',
    'application/vnd.rar',
    'application/x-tar'
  ].includes(mimeType)) {
    return FileType.Archive;
  }
  if ([
    'application/pdf',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'application/msword',
    'application/vnd.ms-excel',
    'application/vnd.ms-powerpoint',
    'application/vnd.oasis.opendocument.text',
    'application/vnd.oasis.opendocument.spreadsheet',
    'application/vnd.oasis.opendocument.presentation'
  ].includes(mimeType)) {
    return FileType.Document;
  }
  return FileType.Other;
}