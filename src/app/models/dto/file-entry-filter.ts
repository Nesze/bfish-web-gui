export interface FileEntryFilter {
  name?: string;
  extension?: string;
  fullName?: string;
  uploadDateFrom?: Date;
  uploadDateTo?: Date;
  minSize?: number;
  maxSize?: number;
}