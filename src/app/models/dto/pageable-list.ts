export interface PageableList<T> {
  items: T[];
  totalItems: number;
}