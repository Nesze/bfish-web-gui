export interface UserDetails {
    username?: string;
    email?: string;
    apiKey?: string;
}