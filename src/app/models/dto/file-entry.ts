
export interface FileEntry {
  originalName: string;
  extension: string;
  accessName: string;
  uploadDate: Date;
  sizeInBytes: number;
}