export interface FieldErrorResponse {
  field: string;
  cause: FieldErrorType;
}

export enum FieldErrorType {
  INVALID_FORMAT = 'InvalidFormat',
  INVALID_VALUE = 'InvalidValue',
  CONFLICT = 'Conflict'
}
