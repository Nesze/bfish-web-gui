import { Sort } from "@angular/material/sort";

export interface SortingOptionGroup {
  displayName: string;
  icon: string;
  children: SortingOption[];
}

export interface SortingOption {
  displayName: string;
  value: Sort;
}
