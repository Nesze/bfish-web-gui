import { Directive, Output, EventEmitter, HostListener, HostBinding, ElementRef, Input } from '@angular/core';

// export interface FileHandle {
//   file: File,
//   url: SafeUrl
// }

@Directive({
  selector: '[appDropToUpload]'
})
export class DropToUploadDirective {

  @Input()
  overlay?: HTMLElement;

  @Output()
  filesDrop: EventEmitter<FileList> = new EventEmitter();

  constructor() { }

  @HostListener("dragover", ["$event"]) public onDragOver(event: DragEvent) {
    if (event.dataTransfer != null && event.dataTransfer.types[0] === 'Files' && event.dataTransfer.items.length === 1) {
      event.preventDefault();
      event.stopPropagation();
      event.dataTransfer.dropEffect = 'copy';
      this.onFileHoverStart();
    }
  }

  @HostListener("dragleave", ["$event"]) public onDragLeave(event: DragEvent) {
    if (event.dataTransfer != null && event.dataTransfer.types[0] === 'Files') {
      event.preventDefault();
      event.stopPropagation();
      if (event.pageX === 0 && event.pageY === 0) {
        this.onFileHoverEnd();
      }
    }
  }

  @HostListener('drop', ['$event']) public onDrop(event: DragEvent) {
    if (event.dataTransfer != null && event.dataTransfer.types[0] === 'Files') {
      event.preventDefault();
      event.stopPropagation();
      this.onFileHoverEnd();

      this.filesDrop.emit(event.dataTransfer.files);
    }
  }

  //#region Internal event handlers

  onFileHoverStart(): void {
    this.overlay?.classList.add('file-hover');
  }

  onFileHoverEnd(): void {
    this.overlay?.classList.remove('file-hover');
  }

  //#endregion

}
