import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';
import { map, take, filter, flatMap } from 'rxjs/operators';
import { UserDetails } from '../models/dto/user-details';
import { Role } from '../models/role';

@Injectable({
  providedIn: 'root',
})
export class LoginActivate implements CanActivate {

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.userService.isUserLoading().pipe(
      filter(isLoading => !isLoading),
      take(1),
      flatMap(() => this.userService.user$),
      map(user => {
        const allowedRoles = (route.data.allowedRoles as Role[]);
        if (user != null) {
          if (!allowedRoles.includes(Role.USER)) {
            this.router.navigate(['files']);
          }
        } else {
          if (!allowedRoles.includes(Role.GUEST)) {
            this.router.navigate(['login']);
          }
        }
        return true;
      })
    );
  }
}
