import { Pipe, PipeTransform } from "@angular/core";
import { MenuItem, DisplayFor } from '../models/menu-item';
import { UserDetails } from '../models/dto/user-details';

@Pipe({
    name: 'sidenavfilter',
    pure: false
})
export class SidenavFilterPipe implements PipeTransform {
    transform(items: MenuItem[], filter?: UserDetails): any {
        if (!items) {
            return items;
        }
        if (filter != null) {
            return items.filter(item => item.displayFor === DisplayFor.BOTH || item.displayFor === DisplayFor.USER);
        }
        return items.filter(item => item.displayFor === DisplayFor.BOTH || item.displayFor === DisplayFor.GUEST);
    }
}