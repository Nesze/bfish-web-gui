import { BrowserModule, Title  } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import localeHu from '@angular/common/locales/hu';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxFilesizeModule, FileSizePipe } from 'ngx-filesize';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { LayoutModule } from '@angular/cdk/layout';

import { environment } from 'src/environments/environment';
import { UserServiceBaseUrl, UsernameRegExp, PasswordRegExp, FilesServiceBaseUrl, UserBaseUrl, MaxFileSize, ApplicationVersion } from './services/tokens';
import { SidenavFilterPipe } from './utils/sidenav-filter-pipe';

import { SidenavComponent } from './components/sidenav/sidenav.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/authentication/login/login.component';
import { RegistrationComponent } from './components/authentication/registration/registration.component';
import { LoginActivate } from './utils/login-activate';
import { MaterialModule } from './modules/material/material.module';
import { FilesComponent } from './components/files/files.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ConfirmDialogComponent } from './components/dialogs/confirm-dialog/confirm-dialog.component';
import { registerLocaleData } from '@angular/common';
import { FileDetailsDialogComponent } from './components/dialogs/file-details-dialog/file-details-dialog.component';
import { ChangePasswordComponent } from './components/profile/change-password/change-password.component';
import { ChangeEmailComponent } from './components/profile/change-email/change-email.component';
import { FooterComponent } from './components/footer/footer.component';
import { DropToUploadDirective } from './directives/drop-to-upload.directive';
import { ForgottenPasswordComponent } from './components/authentication/forgotten-password/forgotten-password.component';
import { ResetPasswordComponent } from './components/authentication/reset-password/reset-password.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { SplashScreenComponent } from './components/splash-screen/splash-screen.component';
import { AboutComponent } from './components/about/about.component';
import { FilePreviewComponent } from './components/files/file-preview/file-preview.component';
import { FileTypePipe } from './pipes/file-type/file-type.pipe';

registerLocaleData(localeHu);

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    LoginComponent,
    HomeComponent,
    SidenavFilterPipe,
    RegistrationComponent,
    FilesComponent,
    ProfileComponent,
    ConfirmDialogComponent,
    FileDetailsDialogComponent,
    ChangePasswordComponent,
    ChangeEmailComponent,
    FooterComponent,
    DropToUploadDirective,
    ForgottenPasswordComponent,
    ResetPasswordComponent,
    SplashScreenComponent,
    AboutComponent,
    FilePreviewComponent,
    FileTypePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxFilesizeModule,
    ClipboardModule,
    LayoutModule,
    MaterialModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [
    Title,
    LoginActivate,
    {
      provide: LOCALE_ID,
      useValue: 'hu'
    },
    {
      provide: ApplicationVersion,
      useValue: environment.applicationVersion
    },
    {
      provide: UserServiceBaseUrl,
      useValue: `${environment.protocol}://${environment.domain}/${environment.apiUrl}/${environment.userBaseUrl}`
    },
    {
      provide: FilesServiceBaseUrl,
      useValue: `${environment.protocol}://${environment.domain}/${environment.apiUrl}/${environment.filesBaseUrl}`
    },
    {
      provide: UserBaseUrl,
      useValue: `${environment.protocol}://<user>.${environment.domain}`
    },
    {
      provide: UsernameRegExp,
      useValue: environment.usernameRegExp
    },
    {
      provide: PasswordRegExp,
      useValue: environment.passwordRegExp
    },
    {
      provide: MaxFileSize,
      useValue: environment.maxFileSize
    },
    FileSizePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
